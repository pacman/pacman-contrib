paclog-pkglist(8)
=================

Name
----
paclog-pkglist - list currently installed packages based on pacman's log


Synopsis
--------
'paclog-pkglist' [path to pacman log]


Description
-----------
'paclog-pkglist' will parse a log file into a list of currently installed packages.
Example: paclog-pkglist /var/log/pacman.log
Defaults to /var/log/pacman.log


Options
-------
*-h, \--help*::
	Display syntax and command-line options.

*-V, \--version*::
	Display version information.


See Also
--------
linkman:pacman[8]

include::footer.adoc[]
